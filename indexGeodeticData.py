# -*- coding: utf-8 -*-
# Code managed by Jean-Luc Menut
# OCA/CNRS

"""indexGeodeticData.py - indexGD - is a program that indexes GNSS data by collecting
   information from data files and sending the results to a FWSS web services."""

import argparse
import shutil
import hashlib
import json
import os
import os.path
import signal
import subprocess
import sys
import time
from builtins import input, str
from datetime import datetime
from os.path import basename, dirname, isfile
from urllib.parse import urlparse, urlunparse

import requests

import timefunc as tf

__VERSION__ = "1.2.7"

FWSS_VERSION_COMPATIBILITY = "1.3.x"

__PYTHON_VERSION__ = sys.version_info

CRX2RNX = "CRX2RNX"  # path for CRX2RNX


def is_tool(name):
    """
    From https://stackoverflow.com/questions/11210104/check-if-a-program-exists-from-a-python-script/34177358
    """
    return shutil.which(name) is not None


# This function mostly from:
# https://stackoverflow.com/questions/1131220/get-md5-hash-of-big-files-in-python#answer-1131255


def generate_file_md5(filepath, blocksize=2**20):
    """
    Take a filepath, calculate MD5 hash for file,
    return hash in 32 hex digit string
    """
    m = hashlib.md5()
    with open(filepath, "rb") as f:
        while True:
            buf = f.read(blocksize)
            if not buf:
                break
            m.update(buf)
    return m.hexdigest()


def scan_path(args):
    """Scan through directory structures for files according to passed
    arguments and return a list of file candidates to be scanned."""

    file_path_list = []
    for path, dirs, files in os.walk(args.path):
        for file in files:
            file_path = os.path.join(path, file)

            # Define the file_mask flag
            file_mask_matches = False

            # Process the file mask, if given.
            if args.file_mask:
                patterns = args.file_mask.split("*")

                match = True

                for pattern in patterns:
                    if pattern in file:
                        pass
                    else:
                        match = False
                if match:
                    file_mask_matches = True

            else:
                file_mask_matches = True

            # FOR THE TIME BEING, THIS FILTER IS ALWAYS ON
            # TODO: Revise this filtering post-validation.
            split_extension = os.path.splitext(file_path)
            if len(split_extension) == 2:
                extension = split_extension[1]
            else:
                extension = None

            if extension.lower() in [".z", ".gz", ".7z"] and file_mask_matches:
                file_path_list.append(file_path)

    return file_path_list


def scan_files(file_path_list, args):
    """Scan files, given a list, parse the filenames,
    call the filesystem for dates and create JSON indexing object.
    Send the indexing object to a FWSS web service."""

    # remove the file if present before the processing
    # because we are going to append the json string for each iteration

    # count the number of files. Used to remove the last  comma
    # in the final json to comply with the JSON python parser
    cnt_files = 0

    # Optionnal imports
    if args.web_service_path:
        import requests

    if args.output_file and isfile(args.output_file):
        os.remove(args.output_file)

    # dictionnary to store the log of errors/process
    log_file_dict = {}

    # File signatures - file headers: in this case are the first 16 bites of a file
    # can give an idea what the file is.
    file_signatures = {
        "1f9d": "a .Z file",  # standard (Lempel-Ziv-Welch) compression
        "1f8b": "a gzip file",
        "377a": "a .7z file",  # not used yet
        "504b": "a .zip file",  # gunzip uncompress zip file too
    }
    for file_path in file_path_list:
        try:
            file_handle = open(file_path, "rb")
        except Exception as err:
            print("ERROR: {0}".format(err))
            print(
                "ERROR:  File list does not seem to be a list of files... Please check your input file."
            )
            sys.exit(0)
        try:
            # Read the first 2 bytes from the file
            byte = file_handle.read(2)
            # Encode it as hex... reads better.
            if __PYTHON_VERSION__.major == 2:
                hex_line = byte.encode("hex")
            else:
                hex_line = byte.hex()
            # Get full path for our file
            abs_path = os.path.abspath(file_path)

            # Empty files occurr. Let's not let it crash our code
            if not hex_line:
                hex_line = "gggg"

            # Check if header pattern is in our dictionary and report
            try:
                # If the file has a certain compression, recognizable from the file signature,
                # it is then probably a Rinex file.
                if hex_line in file_signatures:
                    # Parse file name and path
                    print(">> Path to files: {0}".format(abs_path))
                    print(">> Path to web service: {0}".format(args.web_service_path))

                    md5sum = generate_file_md5(abs_path)
                    # Get all system files informations in one time
                    # This is needed for creation_date, revision_date and file_size
                    file_stat = os.stat(abs_path)
                    # Also require uncompressed RINEX file's md5sum!
                    # Variant 1:
                    # Create temp uncompressed file, get md5sum,
                    # delete temp file.
                    # Next block commented out in favor of Variant 2.
                    """
                    ctime = datetime.now().strftime("%Y%m%dT%H%M%S")
                    temp_path = '/tmp/' + ctime + '_rinex.tmp'
                    sys_cmd_unpack = ('gunzip -c ' + abs_path +
                                      ' | CRX2RNX > ' + temp_path)
                    subprocess.check_call(sys_cmd_unpack, shell=True)
                    if os.path.isfile(temp_path):
                        md5uncompressed = generate_file_md5(temp_path)
                        os.remove(temp_path)
                    else:
                        print 'Error: Unpacking RINEX and creating temp failed!'
                        sys.exit(1)
                    """
                    #
                    # Variant 2:
                    # Read from pipe of uncompressed RINEX file.
                    # No blocked processing, so only ok for 'small' files...
                    if is_tool("pigz"):
                        sys_cmd_unpack = "pigz -d -c " + abs_path + " | " + CRX2RNX
                    else:
                        sys_cmd_unpack = "gunzip -c " + abs_path + " | " + CRX2RNX
                    try:
                        # do not change the format to keep md5uncompressed consistent
                        rinex_content = subprocess.check_output(
                            sys_cmd_unpack, shell=True
                        )
                    except:
                        print(
                            "ERROR: the file {0} cannot be decompressed".format(
                                abs_path
                            )
                        )
                    md5uncompressed = hashlib.md5(rinex_content).hexdigest()
                    #

                    if args.file_type == "AUTO":
                        header = rinex_content[:1000].decode().splitlines()
                        for l in header:
                            if l.find("RINEX VERSION") > -1:
                                try:
                                    r_vers = l.split()[0].strip()[
                                        0
                                    ]  # read the first character
                                except Exception as err:
                                    print(
                                        "Unable to read the RINEX VERSION for the files"
                                    )
                                    break
                                if float(r_vers) < 3:
                                    args.file_type = "RINEX2"
                                elif float(r_vers) < 4:
                                    args.file_type = "RINEX3"
                                elif float(r_vers) < 5:
                                    args.file_type = "RINEX4"
                                break
                            else:
                                print("Unable to read the RINEX VERSION for the files ")
                    if args.file_type in ["RINEX2", "RINEXN2", "RINEXM2"]:
                        file_name = basename(abs_path)
                        # if len(file_name.split('.')[0]) == 8:

                        # test if RINEX 2 30s file or
                        if args.sampling_window == "24h":
                            if file_name[7] in "0123456789":
                                marker = file_name[0:4]
                                year = file_name[9:11]
                                # will work until 2080 only... but the rinex file name also
                                if float(year) > 80:
                                    year = "19" + year
                                else:
                                    year = "20" + year
                                day_of_year = file_name[4:7]
                                path = dirname(abs_path)
                                # Strip the station ID from the Rinex file name
                                # make the date readable whatever the file
                                rinex_date = year + "-" + day_of_year

                                # Transform Rinex date format to UTC format
                                reference_date = tf.currDate(
                                    refday=tf.toDatetime(rinex_date, "%Y-%j"),
                                    String="%Y-%m-%d",
                                )
                            else:
                                print(
                                    "ERROR: File name {0} is not at the format 30s".format(
                                        abs_path
                                    )
                                )

                        elif args.sampling_window == "1h":
                            if file_name[7] in "abcdefghijklmnopqrstuvwx":
                                marker = file_name[0:4]
                                year = file_name[9:11]
                                # will work until 2080 only... but the rinex file name also
                                if float(year) > 80:
                                    year = "19" + year
                                else:
                                    year = "20" + year
                                day_of_year = file_name[4:7]
                                path = dirname(abs_path)
                                # Strip the station ID from the Rinex file name
                                rinex_date = year + "-" + day_of_year
                                # Transform Rinex date format to UTC format
                                reference_date = tf.currDate(
                                    refday=tf.toDatetime(rinex_date, "%Y-%j"),
                                    String="%Y-%m-%d",
                                )
                                reference_date += " %02d:00:00" % (
                                    ord(file_name[7]) - 97
                                )
                            else:
                                print(
                                    "ERROR: File name {0} is not at the format 1s".format(
                                        abs_path
                                    )
                                )
                        # else:
                        #     file_name = basename(abs_path)
                        #     marker = file_name[0:4]
                        #     year = file_name[12:16]
                        #     day_of_year = file_name[16:19]
                        #     HH = file_name[19:21]
                        #     MM = file_name[21:23]
                        #     path = dirname(abs_path)
                        #     rinex_date = year + '-' + day_of_year
                        #     # Transform Rinex date format to UTC format
                        #     reference_date = tf.currDate(refday=tf.toDatetime(
                        #         rinex_date, "%Y-%j"), String='%Y-%m-%d')
                        #     reference_date += " %s:%s:00" % (HH, MM)

                        # relative_path = relative_path.replace('%WWWW%', date_to_gpsweek(year, doy)) # TODO

                    if args.file_type in ["RINEX3", "RINEXN3", "RINEXM3", "RINEX4"]:
                        file_name = basename(abs_path)
                        marker = file_name[0:4]
                        year = file_name[12:16]
                        day_of_year = file_name[16:19]
                        HH = file_name[19:21]
                        MM = file_name[21:23]
                        path = dirname(abs_path)
                        rinex_date = year + "-" + day_of_year
                        # Transform Rinex date format to UTC format
                        reference_date = tf.currDate(
                            refday=tf.toDatetime(rinex_date, "%Y-%j"), String="%Y-%m-%d"
                        )
                        reference_date += " %s:%s:00" % (HH, MM)

                        # relative_path = relative_path.replace('%WWWW%', date_to_gpsweek(year, doy)) # TODO

                    if args.relative_path:
                        calendar_year = datetime.strptime(
                            year + "-" + day_of_year, "%Y-%j"
                        )
                        month = str(calendar_year.month).zfill(2)
                        day = str(calendar_year.day).zfill(2)

                        relative_path = args.relative_path
                        relative_path = relative_path.replace("%YYYY%", year)
                        relative_path = relative_path.replace("%DOY%", day_of_year)
                        relative_path = relative_path.replace(
                            "%CODEUP%", marker.upper()
                        )
                        relative_path = relative_path.replace(
                            "%CODELO%", marker.lower()
                        )
                        relative_path = relative_path.replace("%LOCAL_PATH%", path)
                        relative_path = relative_path.replace("%DD%", day)
                        relative_path = relative_path.replace("%MM%", month)

                    else:
                        # If relative path is not given, the empty string is used
                        # The relative_path variable cannot be empty in the database.
                        relative_path = ""

                    # Transform datetime info in file_name into reference date
                    # 24 hour file example: KIDC3360.15D.Z

                    # CHECK ARGUMENTS

                    # Check if file type info was given

                    if args.file_type:
                        file_type = args.file_type
                    else:
                        file_type = None

                    # Check if sampling window info was given.
                    if args.sampling_window:
                        sampling_window = args.sampling_window
                    else:
                        sampling_window = None

                    # Check if sampling frequency info was given.
                    if args.sampling_frequency:
                        sampling_frequency = args.sampling_frequency
                    else:
                        sampling_frequency = None

                    # Check if data center info was given.
                    if args.data_center:
                        data_center = args.data_center
                    else:
                        data_center = "N/N"

                    # If we have no file_type information,then we will turn to eudcated guessing
                    if not file_type:
                        # If the file_type is not given we aslo assume that sampling_window and
                        # sampling_frequency were not given either.

                        # Initialise our file mask
                        file_mask = []

                        # Our known rinex masks: 1 if the character in the file name is
                        # alphabetical, 0 if not. These are just the first file masks.
                        # RINEX3 and the rest also needs to be defined.
                        # Why not use regular expressions: because they eat alot of memory
                        # and slow the process down.

                        rinex_24hr_mask = [1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1]
                        rinex_xhr_mask = [1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1]

                        # Get file mask from file name
                        for char in file_name:
                            if char.isalpha():
                                file_mask.append(1)
                            else:
                                file_mask.append(0)

                        # Compare our file mask to known file masks.
                        # More file masks need to be added gradually.
                        if file_mask == rinex_24hr_mask:
                            file_type = "RINEX2"
                            sampling_window = "24hour"
                        elif file_mask == rinex_xhr_mask:
                            file_type = "RINEX2"
                            sampling_window = "N/N"
                        else:
                            file_type = "N/N"
                            sampling_window = "N/N"

                        if args.sampling_frequency:
                            sampling_frequency = args.sampling_frequency
                        else:
                            sampling_frequency = "N/N"

                    # Send info to service
                    body = json.dumps(
                        {
                            "station_marker": marker.upper(),
                            "file_name": file_name,
                            "file_type": file_type,
                            "sampling_window": sampling_window,
                            "sampling_frequency": sampling_frequency,
                            "relative_path": relative_path,
                            "data_center": data_center,
                            "reference_date": reference_date,
                            # Get current date and time for published_date
                            "published_date": datetime.now().strftime(
                                "%Y-%m-%d %H:%M:%S"
                            ),
                            "revision_date": datetime.fromtimestamp(
                                file_stat.st_mtime
                            ).strftime("%Y-%m-%d %H:%M:%S"),
                            "creation_date": datetime.fromtimestamp(
                                file_stat.st_mtime
                            ).strftime("%Y-%m-%d %H:%M:%S"),
                            "file_size": file_stat.st_size,
                            "md5_checksum": md5sum,
                            "md5uncompressed": md5uncompressed,
                        }
                    )

                    print(">> JSON body: ", body)

                    # Post the information
                    if args.output_file:
                        cnt_files += 1
                        with open(args.output_file, "a") as out_file:
                            out_file.write(body + "\n")

                    if args.web_service_path:
                        r = requests.post(args.web_service_path, data=body)

                        print(
                            ">> File posted: {0} Status code: {1} Reply from service: {2}\n".format(
                                file_name, r.status_code, r.text
                            )
                        )
                        log_file_dict[file_name] = {
                            "action": "upload",
                            "file_path": file_path,
                            "status_code": r.status_code,
                            "reason": r.text,
                        }

            except Exception as err:
                # print "{0} is a unknown file with header {1}".format(abs_path, hex_line)
                raise err

        finally:
            file_handle.close()

    if args.log_dir:
        log_dir = args.log_dir
    else:
        log_dir = os.getcwd()
    # cnt_files += 1
    if args.log_name:
        log_name = args.log_name
    else:
        log_name = "session_{DATE}.json".format(
            DATE=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        )
    if log_file_dict:
        with open(os.path.join(log_dir, log_name), "w") as log_file_output:
            log_file_output.write(json.dumps(log_file_dict))

    if args.output_file:
        # transform the list of json to a correct entire json

        import tempfile

        directory_name = os.path.dirname(args.output_file)
        # without undelete file is deleted before being able to copy it
        with tempfile.NamedTemporaryFile(
            mode="w", dir=directory_name, delete=False
        ) as tmp_file:
            tmp_file.write("[\n")

            if os.path.exists(args.output_file):
                with open(args.output_file) as out_file:
                    for cpt, l in enumerate(out_file):
                        # cnt_files  is the number of files and doesn't start at 0 but 1
                        if cpt < (cnt_files - 1):
                            tmp_file.write(l.rstrip() + ",\n")
                        else:
                            tmp_file.write(l.rstrip() + "\n")
                tmp_file.write("]\n")
                os.rename(tmp_file.name, args.output_file)

            else:
                print(">> File {0} not generated\n".format(args.output_file))


def prog_info_screen():
    """Program splash screen."""

    today = datetime.now().strftime("%A %d. %B %Y - %H:%M:%S")

    print("")
    print("# {} #".format(today))
    print("")


def help_screen(parser):
    """Program help screen."""
    parser.print_help()
    print("")
    # web service optionnal
    print(
        "indexGeodeticData.py: error: FILE PATH or FILE LIST must be provided at minimum, or --upload-only selected."
    )
    sys.exit(0)


def exit_gracefully(signum, frame):
    # restore the original signal handler as otherwise evil things will happen
    # in raw_input when CTRL+C is pressed, and our signal handler is not re-entrant
    signal.signal(signal.SIGINT, original_sigint)

    try:
        if input("\nReally quit? (y/n)> ").lower().startswith("y"):
            sys.exit(1)

    except KeyboardInterrupt:
        print("Ok ok, quitting")
        sys.exit(1)

    # restore the exit gracefully handler here
    signal.signal(signal.SIGINT, exit_gracefully)

    # Method borrowed from:
    # http://stackoverflow.com/questions/18114560/python-catch-ctrl-c-command-prompt-really-want-to-quit-y-n-resume-executi


def check_version(url):
    check_dict = {}
    parsed = urlparse(url)
    new_url = urlunparse((parsed.scheme, parsed.netloc, "/checkversion", "", "", ""))
    try:
        res = requests.get(new_url)
        if res.status_code != 200:
            print("Unable to reach", new_url)
            sys.exit(0)

    except:
        print("Unable to reach", new_url)
        sys.exit(0)

    if float(res.json()["version"]) == FWSS_VERSION_COMPATIBILITY:
        return True
    else:
        return False


def main():
    """Main function."""

    # This is used to catch Ctrl-C exits
    original_sigint = signal.getsignal(signal.SIGINT)
    signal.signal(signal.SIGINT, exit_gracefully)

    # Display some nice program info
    prog_info_screen()

    # Instantiate argparser
    parser = argparse.ArgumentParser()

    # Setup the argument parser
    # parser.add_argument('-i','--full_index',
    #                    action='store_true',
    #                    help='''Full index tries to send info on all files to the indexing web service.
    #                            The default setting is incremental indexing.''')
    # parser.add_argument('-f','--file_type',
    #                    type=str,
    #                    default='rinex',
    #                    help='''Choose to index specific types of geodetic files, (1) "raw", (2) "rinex".
    #                            "rinex" is the default.''')
    parser.add_argument(
        "-p-", "--path", metavar="PATH", type=str, help="""Path to data files"""
    )
    parser.add_argument(
        "-w",
        "--web_service_path",
        type=str,
        help="""Path to the web service that receives the information
                                (e.g. -w http//localhost:5000/gps/data/rinex)""",
    )
    parser.add_argument(
        "-t",
        "--file_type",
        type=str,
        default=None,
        help="""Information on the file type being processed. Current file types are:
                                RINEX2, RINEX3, RINEXN2, RINEXN3, RINEXM2, RINEXM3""",
    )
    parser.add_argument(
        "-s",
        "--sampling_window",
        type=str,
        default=None,
        help="""Common sampling windows are: 24h, 1h, 15mn""",
    )
    parser.add_argument(
        "-f",
        "--sampling_frequency",
        type=str,
        default=None,
        help="""Common sampling frequencies are: 30s, 15s, 1s""",
    )
    parser.add_argument(
        "-d",
        "--data_center",
        type=str,
        default=None,
        help="""Data center acronyms are here the common currency. E.g.
                                'IMO' for the Icelandic Met Office""",
    )
    parser.add_argument(
        "-m",
        "--file_mask",
        type=str,
        default=None,
        help="""File mask can be in the Comment ça va ?form of "*17D* or "GOPE*.Z" or similar to
                                filter out and index files that contain certain keywords""",
    )
    parser.add_argument(
        "-l",
        "--list_files",
        type=str,
        default=None,
        help="""list of rinex files to process""",
    )

    parser.add_argument(
        "-o", "--output_file", type=str, default=None, help="""json output file"""
    )
    parser.add_argument(
        "--log_dir", type=str, default=None, help="""Directory to store the logs"""
    )
    parser.add_argument(
        "--log_name", type=str, default=None, help="""Filename of the json log"""
    )
    parser.add_argument(
        "-r",
        "--relative_path",
        type=str,
        default=None,
        help="""relative path of the file on the datacenter.
                                Ex: if the URL is ftp://renag.unice.fr/data/2017/005/soph0050.17d.Z
                                the relative path is %%YYYY%%/%%DOY%%

                                Available patterns are :
                                    %%YYYY%% : year on 4 digits,
                                    %%DOY%% : day of year on 3 digits,
                                    %%MM%% : month in 2 digits,
                                    %%DD%% : day in 2 digits,
                                    %%CODELO%%: station marker in lowercase,
                                    %%CODEUP%%: station marker in uppercase,
                                    %%LOCAL_PATH%% : the local path,
                                """,
    )  # % is an excape character so %% is %
    parser.add_argument(
        "--upload-only",
        type=str,
        default=None,
        help="""upload a previoulsy generated json to the web service.
                        TO be used the option -w only""",
    )

    parser.add_argument(
        "--check-version",
        action="store_true",
        default=None,
        help="""Check if this version of indexGD is compatible with DB-API""",
    )

    parser.add_argument(
        "--version", action="store_true", default=None, help="""Display version"""
    )

    # Fetch the arguments
    args = parser.parse_args()

    # check if CRX2RNX is installed
    check_hatanaka()

    if args.version:
        print("Version:", __VERSION__)
        sys.exit(0)
    # Check if all arguemts are
    # if not args.path and not args.web_service_path:
    if (
        not args.path
        and not args.list_files
        and not args.upload_only
        and not args.check_version
    ):  # web service optionnal
        help_screen(parser)

    # check DB-API version
    if args.web_service_path and args.check_version:
        if not check_version(args.web_service_path):
            print(
                "IndexGD Version is incompatible with this DB-API version.\nThis indexGD version is compatible only with DB-API version",
                FWSS_VERSION_COMPATIBILITY,
            )
            sys.exit(1)
        else:
            print(
                "IndexGD Version is compatible with this DB-API version (%s)."
                % (FWSS_VERSION_COMPATIBILITY)
            )
            sys.exit(0)

    if args.upload_only:
        if not check_version(args.web_service_path):
            print(
                "IndexGD Version is incompatible with this DB-API version.\nThis indexGD version is compatible only with DB-API version",
                FWSS_VERSION_COMPATIBILITY,
            )
            sys.exit(1)

        for key in list(vars(args).keys()):
            if (
                key != "upload_only"
                and key != "web_service_path"
                and vars(args)[key] != None
            ):
                print("--upload-only should be used without any other option")
                sys.exit(1)

        if not args.web_service_path:
            print("--upload-only needs the path to the web service to be defined")
            sys.exit(1)

        with open(args.upload_only) as f:
            # json may be too big to be read in memory. we parse the json line per line instead
            for line in f:
                if line.strip():
                    if line[0] not in ("[", "]"):
                        if line[-1] == ",":
                            line = line[-1]
                        r = requests.post(args.web_service_path, data=line)
                        print(
                            ">> Line posted {0} Status code: {1} Reply from service: {2}\n".format(
                                line, r.status_code, r.text
                            )
                        )

    # Check if file type is valid
    # Be careful it has to be updated along the database
    else:
        # if not check_version(args.web_service_path):
        #     print 'IndexGD Version is incompatible with this DB-API version.\nThis indexGD version is compatible only with DB-API version', FWSS_VERSION_COMPATIBILITY
        #     sys.exit(0)
        if args.file_type not in [
            "RINEX2",
            "RINEX3",
            "RINEX4",
            "AUTO",
        ]:
            print("File type invalid")
            sys.exit(1)

        if not args.data_center:
            print("A data center acronym is mandatory")
            sys.exit(1)
        # Check if window sampling type is valid
        # Be careful it has to be updated along the database
        if args.sampling_window not in ["24h", "1h", "15mn"]:
            print("Window sampling invalid")
            sys.exit(1)

        # Set timer for the operation
        timer_start = time.time()

        if args.path:
            file_list = scan_path(args)
        elif args.list_files:
            try:
                file_list = open(args.list_files).read().splitlines()
            except Exception as err:
                print(">> {0}".format(err))
                print(">> File corrupted or missing. Please check your input file.")
                sys.exit(1)

        scan_files(file_path_list=file_list, args=args)

        timer_stop = time.time()
        time_ended = datetime.now().strftime("%H:%M:%S")

        print("-----------------------------------------------------------------------")
        print(
            "Indexing geodetic files finished {0}. Time elapsed: {1:.1f} seconds".format(
                time_ended, timer_stop - timer_start
            )
        )
        print("-----------------------------------------------------------------------")


def check_hatanaka():
    if not is_tool(CRX2RNX):
        print("CRX2RNX (Hatanaka tool) is not installed")
        sys.exit(1)


if __name__ == "__main__":
    main()
