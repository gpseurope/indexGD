# 1. Introduction #
The indexGeodeticData.py program generate file metadata as JSON.
The resulting JSON can be saved and.or send to a Web Services Server for populating a database.

**Authors:** 
* Jean-Luc Menut (Jean-Luc.MENUT_at_geoazur.unice.fr)
* Juliette Legrand (juliette.legrand_at_oma.be)


**Previous Authors**
* Fjalar Sigurðarson (fjalars@gmail.com / fjalar@vedur.is)


**Date created:** 21. September 2017

**Date last update:** 25 Jan. 2024

**version : 1.2.7**

# 2. Requirements #
## Python librairies

* python 3.10
* pytz
* numpy
* python-dateutil

The required librairies can be installed with `pip install -r requirement.txt.py`

## Unix tools
* gzip
* pigz  (optionnal, parrallel gzip)
* compress
* CRX2RNX (Hatanaka decompressor)

# 3. Usage #

Display options with:
```bash
python indexGeodeticData.py -h
```



### Mandatory parameters: ###

* -p: path to the directory of the files to index. Ex: `-p /ftp/data`

_OR_

* -l: a file listing the path to the files to index. Ex: `-l rinex_list.txt`


### Output parameters: ###

*  -w : Web Services Server URL. Ex: `-w http//localhost:5000/gps/data/rinex`

_OR_

* -o: output file. Ex: `-o 2017-09-21_processed.json`



###  RINEX parameters: ###

* -t:  File type. **Available** : RINEX2, RINEX3, RINEX4, AUTO (AUTO Detect the RINEX version)
* -s: Sampling window. **Available**: 24h, 1hr
* -f: Sampling frequency. **Available**:  30s, 1s


##### Information about the file #####

##### Other options #####
* -h: Display help and exit
* -d: The shortname for the Data Center. Ex: `-d GA`.
* -m: (optional) Mask to filter the file found in directories. Ex: `-m "*17D*` or `-m "GOPE*.Z"
* -r: Relative path to add between protocol+hostname+directory_naming and the file name to build the URL linking the file. Pattern can be used.


   **Available patterns** :
   - %YYYY% : year on 4 digits,
   - %DOY% : day of year on 3 digits,
   - %CODELO%: station marker in lowercase,
   - %CODEUP%: station marker in uppercase,
   - %LOCAL_PATH% : the local path,



* --upload-only: upload a previously generated JSON file to the Web Services Server.
* --version: Display the version and exit
* -log_dir: (optional) Directory where the log files will be stored.

# 4. Examples #

We have files in the format `sophDOY0.YYd.Z`  in the directory `/ftp/pub/data/YYYY/DOY`
where YY is the year on 2 characters, YYYY is the year on 4 characters and DOY the Day of the Year.

For axample the file `soph0050.17d.Z` has for path on the hard drive `/ftp/pub/data/2017/005/sophDOY0.YYd.Z``

The actual URL of this file on the FTP is different and is ftp://renag.unice.fr/data/2017/005/soph0050.17d.Z


For generating a JSON of the metadata for the file in these directory, the command line will be:

```bash
python  indexGeodeticData.py -p /ftp/pub/data -t RINEX2 -s 24h -f 30s -d GA -o 2017-09-21_processed.json -r %YYYY%/%DOY%
```

* `-p /ftp/pub/data` the directory where the rinex files are stored
* `-t RINEX2 -s 24h -f 30s ` : it's a RINEX 2  24h file with a sampling of 30s
* `-d GA` : the Datacenter is Geoazur. **Note:** The Data Center information allowing to build the URL need to be inserted in the DB first. In our example it is ftp://renag.unice.fr/data/
* `-o 2017-09-21_processed.json`: no upload to a database, the JSON is saved in a file
* `-r %YYYY%/%DOY%` : because the `-d` option provides the address and the structure of the data center
(previously stored), the relative path is the only part missing
 between the addres++structure and the name of the file.
 Here we use a pattern based on the year, a slash ('/') and the DoY.
 When the program encounter the file  `soph0050.17d.Z`, `%YYYY%` will be replaced
 by `2017` and `%DOY%` by `005`.



If instead of looking in a directory, it is preferable to process a list of files, the `-l` option replace the `-p` option:
```bash
python  indexGeodeticData.py -l list_of_file -t RINEX2 -s 24h -f 30s -d GA -o 2017-09-21_processed.json -r %YYYY%/%DOY%
```

If instead of generating a JSON file, it is preferable to update directly the DB, the `-w` option replace the `-o` option:
```bash
python  indexGeodeticData.py -l list_of_file -t RINEX2 -s 24h -f 30s -d GA -r %YYYY%/%DOY% -w http//localhost:5000/gps/data/rinex
```

# 4. Limitations with current available version

* The program can process both .Z and .gz files.

# 5. Known Issues

* None at the moment....
