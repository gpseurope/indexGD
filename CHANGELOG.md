CHANGELOG
=========
**2024-01-25** Version 1.2.7. Change creation_date to use st_mtime instead of st_ctime in order to not have the bug where the creation date if more recent than the revision date

**2023-12-07** Version 1.2.6. Python 3 only.  Configurable logging file and small updates in the README

**2023-09-26** Version 1.2.5. Add logging of the result when sending the metadata to FWSS. Add RINEX 4. Check if CRX2RNX is installed and allow to configure it's path in the code. Changes in README.md

**2023-09-8** Version 1.2.4. Correct a bug when using AUTO as a file type

**2022-09-29:** Version 1.2.3. Add requirement.txt for both python 2 and 3. The previous changelogs weren't recorded

**2020-10-20:** Manage both pigz and gunzip. Add version number. Add Changelog
